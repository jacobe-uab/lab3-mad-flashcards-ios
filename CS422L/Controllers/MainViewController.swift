//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet var collectionView: UICollectionView!
    var sets: [FlashcardSet] = FlashcardSet().getHardCodedCollection()
    var segueCardTitle = "Title"
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func addSet(_ sender: Any) {
        let newSet = FlashcardSet()
        newSet.title = "I'm new!"
        sets.append(newSet)
        collectionView.insertItems(at: [IndexPath(row: sets.count - 1, section: 0)])
    }
    
    @IBAction func deleteSet(_ sender: Any) {
        if (sets.count > 0) {
            sets.removeLast()
            collectionView.deleteItems(at: [IndexPath(row: sets.count, section: 0)])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDetail" {
            let detail = segue.destination as! FlashcardSetDetailViewController
            detail.setTitle = segueCardTitle
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        segueCardTitle = sets[indexPath.row].title
        performSegue(withIdentifier: "GoToDetail", sender: self)
        

    }
}

