//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Jacob Ellis on 2/8/21.
//

import Foundation
import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var setTitleLabel: UILabel!
    
    var setTitle = "Title"
    
    var cards: [Flashcard] = Flashcard().getHardCodedCollection()

    @IBAction func addCard(_ sender: Any) {
        let newCard = Flashcard()
        newCard.term = "I'm new!"
        newCard.definition = "Hello!"
        cards.append(newCard)
        tableView.insertRows(at: [IndexPath(row: cards.count - 1, section: 0)], with: UITableView.RowAnimation.automatic)
    }
    
    @IBAction func deleteCard(_ sender: Any) {
        if (cards.count > 0){
            cards.removeLast()
            tableView.deleteRows(at: [IndexPath(row: cards.count, section: 0)], with: UITableView.RowAnimation.automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath)
        cell.textLabel?.text = cards[indexPath.row].definition + " - " + cards[indexPath.row].term
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        cards.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitleLabel.text = setTitle
    }
}


